package com.example.marat.myapplication

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.marat.myapplication.Model.ItemGenerator
import com.example.marat.myapplication.Model.MyItem
import com.example.marat.myapplication.Adapters.SelectableAdapter
import com.example.marat.myapplication.Model.Category
import java.util.*

/**
 * Created by MaRaT on 02.11.2016.
 */
class RecyclerViewAdapter(val context: Context,
                          var data: ArrayList<MyItem>,
                          private val recyclerView: RecyclerView,
                          val clickListener: MyViewHolder.ClickListener
                          )
        : SelectableAdapter<RecyclerViewAdapter.MyViewHolder>()

{

    class MyViewHolder : RecyclerView.ViewHolder, View.OnClickListener, View.OnLongClickListener
    {
        var title: TextView? = null
        var text: TextView? = null
        var image: ImageView? = null
        var listener: ClickListener? = null


        constructor(holderView : View, listener_: ClickListener) : super(holderView){
            title = holderView.findViewById(android.R.id.text1) as TextView
            text = holderView.findViewById(android.R.id.text2) as TextView
            image = holderView.findViewById(android.R.id.icon1) as ImageView

            listener = listener_

            holderView.setOnClickListener(this);
            holderView.setOnLongClickListener(this);
        }


        override fun onClick(v: View)
        {
            listener?.onItemClicked(layoutPosition)
        }

        override fun onLongClick(v: View): Boolean
        {
            if (listener != null)
            {
                return listener!!.onItemLongClicked(layoutPosition)
            }
            return false
        }

        interface ClickListener
        {
            fun onItemClicked(position: Int)
            fun onItemLongClicked(position: Int): Boolean
        }
    }

    override fun getItemCount(): Int
    {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder
    {

        var itemView: View = LayoutInflater.from(context).inflate(R.layout.listview_item, parent, false)
        var holder = MyViewHolder(itemView, clickListener)
        return holder
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int)
    {
        if(holder != null)
        {
            val item = data[position]

            holder.title?.text = item.text1
            holder.text?.text = item.text2

            if(item.icon_item != -1)
            {
                holder.image?.visibility = View.VISIBLE
                holder.image?.setImageResource(item.icon_item)
                holder.itemView.isSelected = isSelected(position)
                holder.image?.setOnClickListener {

                    holder.listener?.onItemLongClicked(position)
                }
            }
            else
            {
                holder.image?.visibility = View.INVISIBLE
                //эм
                holder.listener = null
            }
        }
    }


    fun removeItem(position: Int)
    {
        data.removeAt(position)
        notifyItemRemoved(position)
    }

    fun removeItems(positions: List<Int>)
    {
        Collections.sort(positions, Collections.reverseOrder())

        for(indexItem in positions)
        {
            removeItem(indexItem)
            //data.removeAt(indexItem)
        }
    }

    fun selectAllItems()
    {
        for(i in 0..data.size - 1)
        {
            //если не разделитель
            if(data[i].icon_item != -1)
            {
                //setSelectItem(i)
                setSelectItem(i, true)
            }
        }
    }

    fun unSelectAllItems()
    {
        for(i in 0..data.size - 1)
        {
            setSelectItem(i, false)
        }
    }


    fun add(countElement: Int)
    {
        var countGroup = 0
        var index = 1
        var map = HashMap<Int, Pair<Int,ArrayList<MyItem>>>()

        var indexInMap = 0
        for(idx in 0..data.size - 1)
        {
            if(data[idx].icon_item == -1)
            {
                index = idx + 1
                val itemGenerator = ItemGenerator(context, countElement)
                var listItem = itemGenerator.getListItems(countElement, data[idx].group)
                map.put(indexInMap, Pair(index, listItem))
                indexInMap++
            }
        }

        for(i in map.size - 1 downTo 0)
        {
            data.addAll(map[i]!!.first, map[i]!!.second)
        }

    }


    fun findIndexLastElementInGroup(numberGroup: Int):Int?
    {
        for(i in 0..data.size - 1)
        {
            if(data[i].group == numberGroup)
            {
                for(indexElement in i..data.size - 1)
                {
                    if(data[indexElement].group != numberGroup)
                    {
                        return indexElement - 1
                    }
                }
                return data.size - 1
            }
        }
        return null
    }

    fun shuffle()
    {
        var hashMap = HashMap<MyItem, ArrayList<MyItem>>()

        var index = 0
        while(index < data.size)
        {
            if(data[index].icon_item == -1)
            {
                var tempMyItem = data[index]
                var tempArray = ArrayList<MyItem>()

                index++
                while(index < data.size && data[index].icon_item != -1)
                {
                    tempArray.add(data[index])
                    index++
                }

                val seed = System.nanoTime()
                Collections.shuffle(tempArray, Random(seed))
                hashMap.put(tempMyItem, tempArray)
                Log.w("Shuffle", "Запомнил " + tempMyItem.group + " Размер = " + tempArray.size.toString())

            }
        }

        data.clear()
        for((key) in hashMap)
        {
            data.add(key)
            data.addAll(hashMap[key]!!)
            Log.w("Shuffle", "Добавил " + key.group + " Размер = " + hashMap[key]!!.size.toString())
        }
//        val seed = System.nanoTime()
//        Collections.shuffle(data, Random(seed))
    }

    val firstItem: MyItem
        get() = data.first()

    val lastItem: MyItem
        get() = data.last()

}

