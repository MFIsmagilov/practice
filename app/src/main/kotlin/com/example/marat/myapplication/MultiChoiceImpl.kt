//package com.example.marat.myapplication
//
//
//import android.view.*
//import android.widget.AbsListView
//import android.widget.Toast
//
//
///**
// * Created by MaRaT on 31.10.2016.
// */
//class MultiChoiceImpl(private val listView: AbsListView) : AbsListView.MultiChoiceModeListener
//{
//
//    override fun onItemCheckedStateChanged(actionMode: ActionMode, i: Int, l: Long, b: Boolean)
//    {
//        val selectedCount = listView.checkedItemCount
//        setSubtitle(actionMode, selectedCount)
//    }
//
//    override fun onCreateActionMode(actionMode: ActionMode, menu: Menu): Boolean
//    {
//        actionMode.menuInflater.inflate(R.menu.action_mode, menu)
//        return true
//    }
//
//    override fun onPrepareActionMode(actionMode: ActionMode, menu: Menu): Boolean
//    {
//        return false
//    }
//
//    override fun onActionItemClicked(actionMode: ActionMode, menuItem: MenuItem): Boolean
//    {
//        when(menuItem.itemId){
//            R.id.delete_item -> itemMenuDeleteItem(actionMode)
//            R.id.select_all_item -> itemMenuSelectAll(actionMode)
//        }
//
//        return false
//    }
//
//    override fun onDestroyActionMode(actionMode: ActionMode)
//    {
//    }
//
//    private fun setSubtitle(mode: ActionMode, selectedCount: Int)
//    {
//        when (selectedCount)
//        {
//            0 -> mode.subtitle = null
//            else -> mode.title = selectedCount.toString()
//        }
//    }
//
//    fun itemMenuDeleteItem(actionMode: ActionMode)
//    {
//        var adapter = listView.adapter as MyAdapter
//        var checkItemCount = listView.checkedItemCount
//        var indexItem = listView.count - 1
//        while(indexItem > -1)
//        {
//            if(listView.isItemChecked(indexItem) == true)
//            {
//                listView.setItemChecked(indexItem, false)
//                adapter.remove(indexItem)
//
//                checkItemCount--
//                if(checkItemCount == 0)
//                {
//                    break
//                }
//            }
//            indexItem--
//        }
//        adapter.notifyDataSetChanged()
//        if(adapter.count == 0)
//        {
//            actionMode.finish()
//        }
//    }
//
//    fun itemMenuSelectAll(actionMode: ActionMode)
//    {
//        for (i in 0..listView.count - 1)
//        {
//            if(listView.isItemChecked(i) == false)
//            {
//                listView.setItemChecked(i, true)
//            }
//        }
//    }
//}