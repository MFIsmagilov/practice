package com.example.marat.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import com.example.marat.myapplication.Model.Category
import com.example.marat.myapplication.RecyclerViewAdapter
import com.example.marat.myapplication.Model.ItemGenerator
import com.example.marat.myapplication.Model.MyItem
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


open class MainActivity : AppCompatActivity(), RecyclerViewAdapter.MyViewHolder.ClickListener
{

    lateinit var  adapter: RecyclerViewAdapter
    var actionModeCallback = ActionModeCallback()
    var actionMode: ActionMode? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var list:ArrayList<MyItem>
        if(savedInstanceState == null || !savedInstanceState.containsKey("dataListView"))
        {
            val itemGenerator = ItemGenerator(this, 5)
            list = itemGenerator.getListItem()
        }
        else
        {
            list = savedInstanceState.getParcelableArrayList("dataListView")
        }
        val layoutManager = LinearLayoutManager(this)
        val itemAnimator = DefaultItemAnimator()
        adapter = RecyclerViewAdapter(this, list, list_view, this)

        list_view.adapter = adapter
        list_view.layoutManager = layoutManager
        list_view.itemAnimator = itemAnimator
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean
    {
        menuInflater.inflate(R.menu.menu_activity_main, menu)
        return true
    }


    override fun onSaveInstanceState(outState: Bundle)
    {
        outState.putParcelableArrayList("dataListView", (list_view.adapter as RecyclerViewAdapter).data)
        super.onSaveInstanceState(outState)
    }


    fun  itemMenuAdd(item:MenuItem)
    {

        val random = Random()
        val category = Category()

        if(adapter.itemCount == 0)
        {
            val itemGenerator = ItemGenerator(this, 10)
            var list = itemGenerator.getListItem()
            adapter = RecyclerViewAdapter(this, list, list_view, this)
            list_view.adapter = adapter
        }
        else
        {
            adapter.add(1)
        }

        adapter.notifyDataSetChanged()
        //Toast.makeText(this@MainActivity, getString(R.string.added_item) + " " + adapter.lastItem, Toast.LENGTH_SHORT).show()
    }

    fun itemMenuShuffle(item:MenuItem)
    {
        var adapter = list_view.adapter as RecyclerViewAdapter
        adapter.shuffle()
        adapter.notifyDataSetChanged()
    }


    override fun onItemClicked(position: Int)
    {
        if (actionMode != null)
        {
            toggleSelection(position);
        }
        else
        {
            Toast.makeText(this@MainActivity, getString(R.string.position_item) + " " + position, Toast.LENGTH_SHORT).show()
            //adapter.removeItem(position);
        }
    }

    override fun onItemLongClicked(position: Int): Boolean
    {
        if (actionMode == null)
        {
            actionMode = startActionMode(actionModeCallback);
        }
        toggleSelection(position);
        return true;
    }

    protected fun toggleSelection(position: Int)
    {
        adapter.toggleSelection(position)
        val count = adapter.selectedItemCount

        if (count == 0)
        {
            actionMode?.finish()
        }
        else
        {
            actionMode?.title = count.toString()
            actionMode?.invalidate()
        }
    }

    inner class ActionModeCallback : ActionMode.Callback
    {

        override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean
        {
            mode.menuInflater.inflate(R.menu.action_mode, menu)
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean
        {
            return false
        }

        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean
        {
            when(item.itemId){
                R.id.delete_item ->
                {
                    adapter.removeItems(adapter.getSelectedItems());
                    adapter.notifyDataSetChanged()
                    mode.finish();
                    return true;
                }
                R.id.select_all_item ->
                {
                    adapter.selectAllItems()
                    val count = adapter.selectedItemCount
                    actionMode?.title = count.toString()
                    actionMode?.invalidate()
                }
            }
            return false
        }

        override fun onDestroyActionMode(mode: ActionMode)
        {
            adapter.clearSelection()
            actionMode = null
        }
    }
}
