package com.example.marat.myapplication.Model

import android.util.Log
import java.util.*

/**
 * Created by MaRaT on 02.11.2016.
 */

class RangeImage(val min:Int, val max:Int)

class Category
{
    var list: ArrayList<Pair<String, RangeImage>> = arrayListOf()

    constructor()
    {
        list.add(Pair<String, RangeImage>("Emoji Nature Group", RangeImage(0,115)))
        list.add(Pair<String, RangeImage>("Emoji Objects Group", RangeImage(116,345)))
        list.add(Pair<String, RangeImage>("Emoji Places Group", RangeImage(346,445)))
        list.add(Pair<String, RangeImage>("Emoji Smiley Group", RangeImage(446,634)))
        list.add(Pair<String, RangeImage>("Emoji Symbols Group", RangeImage(635,843)))
    }

    fun getCategoryName(number:Int):String
    {
        return list[number].first
    }
    fun getIdImage(numberCategory: Int): Int
    {
        val random = Random()
        var n = list[numberCategory].second.min + random.nextInt(list[numberCategory].second.max - list[numberCategory].second.min)
        Log.w("IdImage", n.toString())
        return n
    }


    val categoryCount:Int
        get() = list.size
}
