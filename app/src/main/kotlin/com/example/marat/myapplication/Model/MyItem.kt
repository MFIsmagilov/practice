package com.example.marat.myapplication.Model

import android.graphics.drawable.Icon
import android.os.Parcel
import android.os.Parcelable
import android.widget.ImageView
import android.widget.TextView
import java.io.BufferedInputStream
import java.io.Serializable
import java.util.*

/**
 * Created by MaRaT on 30.10.2016.
 */

data class MyItem(var text1: String?,
                  var text2: String?,
                  var icon_item: Int,
                  var group: Int): Parcelable
{

    constructor(source: Parcel) : this(source.readString(), source.readString(), source.readInt(), source.readInt())

    override fun describeContents(): Int
    {
        return 0
    }

    override fun writeToParcel(dest: Parcel?, flags: Int)
    {
        dest?.writeString(text1)
        dest?.writeString(text2)
        dest?.writeInt(icon_item!!)
    }

    companion object
    {
        @JvmField final val CREATOR: Parcelable.Creator<MyItem> = object : Parcelable.Creator<MyItem>
        {
            override fun createFromParcel(source: Parcel): MyItem
            {
                return MyItem(source)
            }

            override fun newArray(size: Int): Array<MyItem?>
            {
                return arrayOfNulls(size)
            }
        }
    }
    override fun toString(): String
    {
        return text1.toString()
    }

}
var comp: Comparator<MyItem> = object : Comparator<MyItem>
{
    override fun compare(lhs: MyItem, rhs: MyItem): Int
    {
        return lhs.group - rhs.group
    }
}
