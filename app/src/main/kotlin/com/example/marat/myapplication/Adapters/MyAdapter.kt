//package com.example.marat.myapplication.Adapters
//
//import android.content.Context
//import android.view.*
//import android.widget.*
//import com.example.marat.myapplication.Model.ItemGenerator
//import com.example.marat.myapplication.Model.MyItem
//import com.example.marat.myapplication.R
//import kotlinx.android.synthetic.main.listview_item.view.*
//import java.util.*
//
///**
// * Created by MaRaT on 30.10.2016.
// */
//
//class MyAdapter(val ctx: Context, val data: ArrayList<MyItem>, val listView: ListView) : BaseAdapter()
//{
//    override fun getItem(position: Int): MyItem
//    {
//        return data[position]
//    }
//
//    override fun getItemId(position: Int): Long
//    {
//        return 0
//    }
//
//    override fun getCount(): Int
//    {
//        return data.size
//    }
//
//    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View
//    {
//        var view: View = LayoutInflater.from(ctx).inflate(R.layout.listview_item, null)
//        val item_list = getItem(position)
//
//
//        var text1 = view.findViewById(android.R.id.text1) as TextView
//        text1.text = item_list.text1
//
//        var text2 = view.findViewById(android.R.id.text2) as TextView
//        text2.text = item_list.text2
//
//
//        val image = view.findViewById(android.R.id.icon1) as ImageView;
//        image.setImageResource(item_list.icon_item!!)
//        image.setOnClickListener(object : View.OnClickListener
//        {
//            override fun onClick(v: View)
//            {
//                selectRow(v)
//            }
//
//            private fun selectRow(v: View)
//            {
//                listView.setItemChecked(position, !isItemChecked(position))
//            }
//
//            private fun isItemChecked(pos: Int): Boolean
//            {
//                val sparseBooleanArray = listView.checkedItemPositions
//                return sparseBooleanArray.get(pos)
//            }
//        });
//
//
//
//        return view
//    }
//
//    fun remove(elem: Int)
//    {
//        data.removeAt(elem)
//    }
//
//    fun add(countElement: Int)
//    {
//        val itemGenerator = ItemGenerator(ctx, countElement, report = listView.count)
//        val listItem = itemGenerator.getListItem()
//        data.addAll(listItem)
//    }
//
//    fun add(countElement: Int, numberGroup: Int)
//    {
//        var index = findIndexLastElementInGroup(numberGroup)
//        if (index != null)
//        {
//            index++
//            val itemGenerator = ItemGenerator(ctx, countElement)
//            data.add(index, itemGenerator.getOneItem(index, numberGroup))
//        }
//    }
//
//
//    fun findIndexLastElementInGroup(numberGroup: Int):Int?
//    {
//        for(i in 0..data.size - 1)
//        {
//            if(data[i].group == numberGroup)
//            {
//                for(indexElement in i..data.size - 1)
//                {
//                    if(data[indexElement].group != numberGroup)
//                    {
//                        return indexElement - 1
//                    }
//                }
//                return data.size - 1
//            }
//        }
//        return null
//    }
//
//    fun shuffle()
//    {
//        val seed = System.nanoTime()
//        Collections.shuffle(data, Random(seed))
//    }
//
//    val firstItem: MyItem
//        get() = data.first()
//
//    val lastItem: MyItem
//        get() = data.last()
//
//}