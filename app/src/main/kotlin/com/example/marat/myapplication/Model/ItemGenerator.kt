package com.example.marat.myapplication.Model

import android.content.Context
import android.support.v7.appcompat.R
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import java.util.*

/**
 * Created by MaRaT on 31.10.2016.
 */

class ItemGenerator(val context: Context, val countItem:Int,  val report:Int = 0 )
{
    lateinit var list: ArrayList<MyItem>
    val random = Random()

    val category = Category()


    fun getListItem(): ArrayList<MyItem>
    {
        list = ArrayList<MyItem>()
        var hashList = HashMap<Int, ArrayList<MyItem>>()
        var numberGroup = 0

        for(i in 0..category.categoryCount - 1)
        {
            hashList.put(i, ArrayList<MyItem>())
            hashList[numberGroup]?.add(getTitleItem(numberGroup))
            numberGroup++
        }

        for(i in 0..countItem)
        {
            val numberCategory = i % category.categoryCount
            hashList[numberCategory]?.add(getItemForGroup(i, numberCategory))
        }

        for(_list in hashList.values)
        {
            list.addAll(_list)
        }
        return list
    }


    fun getListItems(count:Int, numberGroup:Int):ArrayList<MyItem>
    {
        var list = ArrayList<MyItem>()
        for(i in 0..count)
        {
            list.add(getItemForGroup(i, numberGroup))
        }
        return list
    }

    fun getItemForGroup(numberItem:Int, numberCategory:Int): MyItem
    {
        val indexImageStr = category.getIdImage(numberCategory)
        var filename ="smile" + indexImageStr
        var res = context.resources.getIdentifier(filename, "drawable", "com.example.marat.myapplication");
        return MyItem("Item " + numberItem.toString(),  "Sub item " + numberItem.toString(), res, numberCategory)
    }

    fun getTitleItem(numberCategory: Int):MyItem
    {
        return MyItem(category.getCategoryName(numberCategory), null, -1, numberCategory)
    }

//
//
//
//    fun addInGroup(number:Int, group:Int): ArrayList<MyItem>
//    {
//        list.add(getOneItem(number, group))
//        return list
//    }
//
//    fun getOneItem(number:Int, group: Int): MyItem
//    {
//        val random = Random()
//        val indexImageStr = random.nextInt(18).toString()
//        var filename ="smile" + indexImageStr + "_" + group.toString()
//        var res = context.resources.getIdentifier(filename, "drawable", "com.example.marat.myapplication");
//        val item = MyItem("Item " + (number + report).toString(),  "Sub item " + (number + report).toString(), res, group)
//        return item
//    }
}